require 'sequel'
require 'fileutils'

unless Dir.exist?("./Storage")
  Dir.mkdir("./Storage")
end

maindb = Sequel.connect(adapter: 'sqlite', database: './Storage/main.db')

maindb.create_table :posts do
  primary_key :id
  String :user
  String :business
  String :title
  String :content
  Integer :review
end

maindb.create_table :businesses do
  primary_key :id
  String :name
  String :type
  Integer :averagereview
  Integer :amountofreviews
end
