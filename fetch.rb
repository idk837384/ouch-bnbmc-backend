require 'faraday'
require 'sequel'

$maindb = Sequel.connect(adapter: 'sqlite', database: './Storage/main.db')
$businesses = $maindb.from(:businesses)

def fetch() 
bnbnav = Faraday.new("https://bnbnav.aircs.racing")

bnbnavData = JSON.parse(bnbnav.get("api/data").body).to_h["landmarks"]
bnbnavData.each do |data|
  data = data[1]
  unless $businesses.where(name: data["name"]).include?($businesses.where(name: data["name"]).order(:id).first)
    $businesses.insert(name: data["name"], type: data["type"], amountofreviews: 1, averagereview: 3)
  else
    print ""
  end
end

bnbaa = Faraday.new("https://bnbaa.aircs.racing")

bnbaaData = JSON.parse(bnbaa.get("api/airlines").body).to_h
bnbaaData.each do |data|
  data = data[1]
  unless $businesses.where(name: data["name"]).include?($businesses.where(name: data["name"]).order(:id).first)
    $businesses.insert(name: data["name"], type: "airline", amountofreviews: 1, averagereview: 3)
  else
    print ""
  end
end
puts "Business data finished being refreshed at: #{Time.now}"
end

Thread.new do
  while 1 == 1 do
    puts "Business data refreshing at: #{Time.now}"
    fetch()
    sleep(172800)
  end
end
