require 'scorched'
require 'sequel'
require 'jwt'

unless File.exist?("./Storage/main.db")
  load './init.rb'
end
load './fetch.rb'

$maindb = Sequel.connect(adapter: 'sqlite', database: './Storage/main.db')
#fetch()
$posts = $maindb.from(:posts)
$businesses = $maindb.from(:businesses)

# API Stuff
class App < Scorched::Controller
  controller do
    get '/data/businesses' do
      $businesses.all.to_json
    end
    
    get '/data/isop' do
      JWT.decode(request.env["HTTP_AUTHORIZATION"], ENV["OUCH_SECRET"], true)[0]["adm"]
    end

    get '/data/alreadyreviewed/*' do |place| 
      if $posts.where(user: JWT.decode(request.env["HTTP_AUTHORIZATION"], ENV["OUCH_SECRET"], true)[0]["sub"], business: place).all != [] 
        resp = true
      else
        resp = false
      end
      resp.to_s
    end

    get '/data/posts' do 
      response.body = $posts.all if (request[:title] == nil) && (request[:business] == nil)
      response.body = $posts.where(title: request[:title]).all if (request[:title] != nil) && (request[:business] == nil)
      response.body = $posts.where(business: request[:business]).all if (request[:title] == nil) && (request[:business] != nil) 
      response.body = $posts.where(business: request[:business], title: request[:title]).all.to_a if (request[:title] != nil) && (request[:business] != nil) 
      response.body.each do |thing|
        thing[:user] = JSON.parse(Faraday.get("https://playerdb.co/api/player/minecraft/" + thing[:user][1..-2]).body).to_h["data"]["player"]["username"]
      end
      response.body.to_json
    end

    post '/create/newpost' do
      if (request[:review].to_i > 5) || (request[:review].to_i < 1)
        afjjfd()
      end
      print '' if (request[:title] + request[:content] + request[:review] + request[:business]); 
      if $posts.where(user: JWT.decode(request.env["HTTP_AUTHORIZATION"], ENV["OUCH_SECRET"], true)[0]["sub"].to_json, business: request[:business]).all == []
        $posts.insert(title: request[:title], content: request[:content], review: request[:review], business: request[:business], user: JWT.decode(request.env["HTTP_AUTHORIZATION"], ENV["OUCH_SECRET"], true)[0]["sub"].to_json)
      else
        afff()
      end  
      revstot = $businesses.where(name: request[:business]).to_a[-1][:amountofreviews].to_i  
      revsavg = $businesses.where(name: request[:business]).to_a[-1][:averagereview].to_i  
      puts "#{revstot} and #{revsavg}" 
      puts $businesses.where(name: request[:business]).to_a 
      $businesses.where(name: request[:business]).update(averagereview: (((revsavg * revstot) + (request[:review].to_i)) / (revstot + 1)), amountofreviews: revstot + 1)  
      response.body
    end

    post '/create/deletepost' do
      uuidtemp = JSON.parse(Faraday.get("https://playerdb.co/api/player/minecraft/" + request[:user]).body).to_h["data"]["player"]["id"]
      uuidtemp = "\"" + uuidtemp + "\""
      puts uuidtemp + " " + JWT.decode(request.env["HTTP_AUTHORIZATION"], ENV["OUCH_SECRET"], true)[0]["sub"]
      if ( ('"' + JWT.decode(request.env["HTTP_AUTHORIZATION"], ENV["OUCH_SECRET"], true)[0]["sub"] + '"' == uuidtemp) || (JWT.decode(request.env["HTTP_AUTHORIZATION"], ENV["OUCH_SECRET"], true)[0]["admin"]) ) 
        $posts.where(user: uuidtemp, business: request[:business]).delete
      else
        [403, {}, ""]
      end
    end
  end
end

run App
